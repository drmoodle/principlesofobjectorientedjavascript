/**
 * Created by appleman on 3/30/14.
 */
// in addition to Object other built-in types are Array, Date, Error, Function, RegExp

var items = new Array();
var now = new Date();
var error = new Error("Something bad happened.");
var func = new Function("console.log('hi');");
var object = new Object();
var re = new RegExp("\\d+");

func();

// Object and Array Literals

var book = {
    name: "The Principles of Object-Oriented JavaScript",
    year: 2014
};
console.log("book  = "+book.name);

var book2 = {
    "name": "The Principles of Object-Oriented JavaScript",
    "year": 2014
};
console.log("book2 = "+book2.name);

// ----- another way to define
var book3 = new Object();
book3.name = "The Principles of Object-Oriented JavaScript";
book3.year = 2014;
console.log("book3 = "+book3.name);

// array literal
var colors = ["red", "blue", "green"];
console.log("colors = "+colors[0]);

var colors2 = new Array("red", "blue", "green");
console.log("colors2 = "+colors2[0]);

// function literals
function reflect(value) {
    return value;
}

// is the same as

var reflect2 = new Function("value", "return value;");

console.log("reflect = "+reflect("this is first"));
console.log("reflect2 = "+reflect2("this is second"));

