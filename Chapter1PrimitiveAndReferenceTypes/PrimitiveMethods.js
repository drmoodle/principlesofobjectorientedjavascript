/**
 * Created by appleman on 3/30/14.
 */
var name = "RANDY";
var lowercaseName = name.toLowerCase();  // convert to lower case
var firstLetter = name.charAt(0);        // get first character
var middleOfName = name.substring(2, 5); // get characters 2-4

console.log("Part 1");
console.log("lowercaseName = "+lowercaseName);
console.log("firstLetter = "+firstLetter);
console.log("middleOfName = "+ middleOfName);


var count = 10;
var fixedCount = count.toFixed(2); // convert to "10.00"
var hexCount = count.toString(16); // convert to "a"

console.log("\nPart 2");
console.log("fixedCount = "+fixedCount);
console.log("hexCount = "+hexCount);


var flag = true;
var stringFlag = flag.toString();

console.log("\nPart 3");
console.log("flag = "+flag);
console.log("stringFlag = "+stringFlag);

// Note primitive values themselves are not objects, despite the fact that they have methods