/**
 * Created by appleman on 3/30/14.
 */

// Reference types are the closes thing in JavaScript to classes.
// THink of JavaScript objects as nothing more than hash tables: key/value

// by conventions constructors begin with capital letter to
// distinguish them from nonconstructor functions

var object1 = new Object();  // Same as in java object holds reference pointer
var object2 = object1;
console.log("object1.toString = "+object1);
console.log("object2.toString = "+object2);

object1 = null; // dereference

