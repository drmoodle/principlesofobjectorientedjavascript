/**
 * Created by appleman on 3/30/14.
 */
//using dot or bracket notation on method

var array1 = [];
array1.push(12345);

var array2 = [];
array2["push"](12345);

console.log("array1 = "+array1);
console.log("array2 = "+array2);

// use a variable
var array3 = [];
var method = "push";
array3[method](12345);
console.log("array3 = "+array3);

//
var items = [];
var object = {};

function reflect(value) {
    return value;
}

console.log(items instanceof Array);
console.log(object instanceof Object);
console.log(reflect instanceof Function);

// identifying Arrays
console.log(Array.isArray(items));