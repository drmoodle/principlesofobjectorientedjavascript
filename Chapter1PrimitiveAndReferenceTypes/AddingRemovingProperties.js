/**
 * Created by appleman on 3/30/14.
 */
// can add/remove properties anytime

var object1 = new Object();  // Same as in java object holds reference pointer
var object2 = object1;

object1.myCustomProperty = "Awesome!";
console.log(object2.myCustomProperty);