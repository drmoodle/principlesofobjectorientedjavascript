/**
 * Created by appleman on 3/30/14.
 */
// 5 primitive types in JavaScript: Boolean, Number, STring, Null, and Undefined

// Strings
console.log("Strings");
var name = "Randy";
var selection = "a";
console.log("name="+name);
console.log("selection="+selection);

// numbers
console.log("\nNumbers");
var count = 25;
var cost = 1.51;
console.log("count="+count);
console.log("cost="+cost);

// boolean
console.log("\nboolean");
var found = true;
console.log("found="+found);

// null
console.log("\nnull");
var object = null;
console.log("object="+object);

// undefined
console.log("\nundefined");
var flag = undefined;
var ref;        // assigned undefined automatically
console.log("flag="+flag);
console.log("ref="+ref);