/**
 * Created by appleman on 3/30/14.
 */
var color1 = "red";
var color2 = color1;

console.log(color1);
console.log(color2);

color1 = "blue";

console.log(color1);
console.log(color2);