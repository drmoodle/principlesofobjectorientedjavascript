/**
 * Created by appleman on 3/30/14.
 */
// Confusing part of JavaScript - concept of primitive wrapper types
//      three primitive wrapper types: String Number and Boolean
// Similar to java - reference types that are automatically created behind the scenes

var name = "Randal";
var firstChar = name.charAt(0);
console.log(firstChar);

// The following is what happens behind the scenes
// What the JavaScript engine does
console.log("\n---Now under the JavaScript Enginee Covers")
var name = "Randal";
var temp = new String(name); // autoboxing then destroy
var firstChar = temp.charAt(0);
temp = null;
console.log(firstChar);

// Try adding a property to a String as if regular object
console.log("\n---Adding property to string")
var name = "Randal";
name.last = "Hanford";
console.log(name.last);
// The following is what javascript doing under the cover
var name = "Randal";
var temp = new String(name);
temp.last = "Hanford";
temp = null;

var temp = new String(name);
console.log(temp.last);
temp = null;

// Testing
console.log("\n---testing if")
var found1 = new Boolean(false);
var found2 = false;

if (found1) {
    console.log("Found1");
}
if (found2) {
    console.log("Found2");
}
console.log(found1);
console.log(found2);