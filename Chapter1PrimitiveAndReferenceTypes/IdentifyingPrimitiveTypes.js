/**
 * Created by appleman on 3/30/14.
 */
console.log(typeof "Randy");    // string
console.log(typeof 10);         // number
console.log(typeof 5.1);        // number
console.log(typeof true);       // boolean
console.log(typeof undefined);  // undefined
console.log("-------------------------\n")

console.log(typeof null);
var value; //set as undefined
console.log(value === null);

var value2 = null;
console.log(value2 === null);
