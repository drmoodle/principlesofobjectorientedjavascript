/**
 * Created by appleman on 3/30/14.
 */
function sayHi() {
    console.log("Hi from sayHi");
}

sayHi();

var sayHi2 = sayHi;

sayHi2();

// function constructor
var sayHi = new Function("console.log(\"Hi number 2\");");

sayHi();
var sayHi2 = sayHi;
sayHi2();

// passing function as argument on method call - in example sort - anonymous function
var numbers = [1, 5, 8, 4, 7, 10, 2, 6];
numbers.sort(function(first, second) {
    return first - second;
});

console.log(numbers);

numbers.sort();
console.log(numbers);
