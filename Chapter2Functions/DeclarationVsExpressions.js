/**
 * Created by appleman on 3/30/14.
 */
// functional declaration
function add(num1, num2) {
    console.log("in add");
    return num1 + num2;
}

// functional expression
var add2 = function(num1, num2) {
    console.log("in add2");
    return num1 + num2;
};

console.log("add ="+add(10, 15));
console.log("add2="+add2(10, 10));

// function declarations are hoisted to top so even though function is declared
//     at bottom it still can be referenced. But function expressions cannot
var result = add3(15, 15);
console.log("add3 result="+result);

function add3(num1, num2) {
    console.log("in add3");
    return num1 + num2;
}
